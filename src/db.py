import sqlite3
import pickle
import json

class DB():
    def __init__(self, fp):
        self.connection = sqlite3.connect(fp)
        self.cursor = self.connection.cursor()

    def commit(self):
        self.connection.commit()

    def close(self):
        self.cursor.close()
        self.connection.close()

    def _get_user_id(self, username):
        r = self.cursor.execute(
            "SELECT id FROM users WHERE username=?",
            [username]
        )
        return r.fetchone()[0]

    def get_all_users(self):
        r = self.cursor.execute(
            "SELECT username FROM users"
        )
        
        return [x[0] for x in r.fetchall()]

    def add_user(self, username):
        self.cursor.execute(
            "INSERT INTO users (username, user_event_counter) VALUES (?, 0)",
            [username]
        )
        self.commit()

    def remove_user(self, username):
        self.cursor.execute("DELETE FROM users WHERE id=?", [self._get_user_id(username)])
        self.commit()
        
    def is_user_new(self, username):
        uid = self._get_user_id(username)
        r = self.cursor.execute(
            "SELECT user_event_counter FROM users WHERE (id=?)",
            [uid]
        )

        return r.fetchone()[0] == 0

    def has_event(self, event_id, username):
        uid = self._get_user_id(username)
        r = self.cursor.execute(
            "SELECT event_id FROM seen_events WHERE (user_id=? AND event_id=?)",
            [uid, event_id]
        )
        return len(r.fetchall()) > 0

    def add_event(self, event_id, username, event_json=None, webhook_error=None):
        uid = self._get_user_id(username)
        r = self.cursor.execute(
            "SELECT user_event_counter FROM users WHERE id=?",
            [uid]
        )

        counter = r.fetchone()[0]

        self.cursor.execute(
            "INSERT INTO seen_events (user_id, event_id, user_event_counter, event_json, webhook_error) VALUES (?, ?, ?, ?, ?)",
            [uid,
            event_id,
            counter + 1,
            None if event_json is None else json.dumps(event_json).encode("utf8"),
            None if webhook_error is None else pickle.dumps(webhook_error)
            ]
        )
        self.commit()

        self.cursor.execute(
            "UPDATE users SET user_event_counter = ? WHERE id=?",
            [counter + 1, uid]
        )
        self.commit()