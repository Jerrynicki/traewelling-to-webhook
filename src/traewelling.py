import asyncio
import datetime
import logging

import aiohttp
import requests
import requests_oauthlib as requests_oa

import config
import db
import webhook_connector

class Traewelling():
    def __init__(self, cfg: config.Config, database: db.DB, wh_connector: webhook_connector.WebhookConnector):
        self.cfg = cfg
        self.db = database
        self.webhook_connector = wh_connector
        self.REFRESH_INTERVAL = cfg.get_key("refresh_interval")

        auth_header = self.cfg.get_key("auth_header")
        self.default_headers = {"Authorization": auth_header}
        # self.client = self.request_oauth()

    def request_oauth(self):
        # https://laravel.com/docs/9.x/passport
        # https://requests-oauthlib.readthedocs.io/en/latest/oauth2_workflow.html#web-application-flow

        ## DOESN'T WORK
        # Currently the auth header yoinked from swagger is used

        client_id = self.cfg.get_key("client_id")
        client_secret = self.cfg.get_key("client_secret")
        redirect_uri = "about:blank"

        oauth_auth_url = self.cfg.get_key("oauth_auth_url")
        oauth_token_url = self.cfg.get_key("oauth_token_url")

        scope = ["read-statuses"]

        oauth = requests_oa.OAuth2Session(client_id, redirect_uri=redirect_uri, scope=scope)

        authorization_url, state = oauth.authorization_url(oauth_auth_url)

        token = oauth.fetch_token(oauth_token_url, client_secret=client_secret, authorization_response=redirect_uri)

        print(token)

    async def refresh_loop(self):
        loop = asyncio.get_event_loop()
        tasks = set()

        logging.info("traewelling mainloop started")
        while True:
            users = self.db.get_all_users()

            logging.info("traewelling mainloop processing for users " + str(users) + " at " + datetime.datetime.now().isoformat())
            
            for username in users:
                t = loop.create_task(self.user_process(username))
                tasks.add(t)
                t.add_done_callback(tasks.discard)

            self.db.commit()
            logging.info("traewelling mainloop processing done at " + datetime.datetime.now().isoformat())
            logging.info("sleeping " + str(self.REFRESH_INTERVAL) + " seconds")
            await asyncio.sleep(self.REFRESH_INTERVAL)

    async def user_process(self, username):
        loop = asyncio.get_event_loop()

        is_new = self.db.is_user_new(username)

        data = await self.get_status_for_user(username)
        if data is None:
            return

        logging.debug(str(data) + " " + username)

        for event in data["data"]:
            if not self.db.has_event(event["id"], username):
                webhook_error = None

                if not is_new:
                    try:
                        logging.info("Requesting stopover info and posting webhook for user " + username + " event id " + str(event["id"]))
                        stopovers_raw = await self.get_stopovers_for_status(event["train"]["trip"])
                        stopovers_raw = stopovers_raw["data"]
                        stopovers = stopovers_raw[list(stopovers_raw.keys())[0]]
                        await self.webhook_connector.post_to_webhook(event, stopovers)
                    except Exception as e:
                        webhook_error = e
                        logging.error("user_process " + username + " " + e)
                
                self.db.add_event(event["id"], username, event, webhook_error)

    async def get_status_for_user(self, username, page=1):
        url_template = self.cfg.get_key("traewelling_get_user_status_url")

        async with aiohttp.ClientSession(headers=self.default_headers) as sess:
            async with sess.get(url_template.format(username=username, page=page)) as response:
                if response.status != 200:
                    text = await response.text()
                    logging.warn("träwelling " + username + " status code " + str(response.status) + " " + text)
                    return None
                return await response.json()

    async def get_stopovers_for_status(self, trip_id):
        # The trip id is contained in data→.→train→trip

        url_template = self.cfg.get_key("traewelling_get_stopovers_url")

        async with aiohttp.ClientSession(headers=self.default_headers) as sess:
            async with sess.get(url_template.format(trip_id=trip_id)) as response:
                if response.status != 200:
                    text = await response.text()
                    logging.warn("träwelling stopovers " + username + " status code " + str(response.status) + " " + text)
                    return None
                return await response.json()